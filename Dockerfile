FROM store/oracle/serverjre:1.8.0_241-b07
LABEL MAINTAINER "SUDIEP123@gmail.com"
ARG ID_TAG
COPY target/assignment-$ID_TAG.jar /opt/
COPY startup.sh /opt/
RUN groupadd -r devops && useradd -r devops -g devops && \
    chown -R devops. /opt/ && \
    chmod +X /opt/assignment-$ID_TAG.jar
USER devops 
WORKDIR /opt/
EXPOSE 8090
ENTRYPOINT ["/bin/sh","/opt/startup.sh"]

